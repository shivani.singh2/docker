```
In this assignment we will try to understand what Deployments and RelicaSets are in K8s.
This time try to deploy OT-Microservice Application using Deployments and ReplicaSets.

```
### Create a docker images and push it to docker hub so that minikube can fetch it.

![image](https://user-images.githubusercontent.com/19822562/148423731-8f538a3f-30bd-4bbf-b582-eb9610612811.
png)

### Dockerhub repository

![image](https://user-images.githubusercontent.com/19822562/148423900-736a2b4a-3b52-47f0-b736-57bc7d29dd39.png)

### create different yml for deployement

![image](https://user-images.githubusercontent.com/19822562/148424042-c3d57031-5f18-4da6-96c1-18dd08ef5ed1.png)
 

![image](https://user-images.githubusercontent.com/19822562/148424379-b0470d22-953b-4775-9494-4baf3661b1a5.png)

